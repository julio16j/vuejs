import api from '../index'

const URL = {
  base: '/api/v4/projects',
  obterPorId: (projetoId) => `${URL.base}/${projetoId}`,
  listarIssues: (projetoId) => `${URL.base}/${projetoId}/issues`
}
export function obterPorId (projetoId) {
  return api.get(URL.obterPorId(projetoId))
}

export function listarIssues (projetoId) {
  return api.get(URL.listarIssues(projetoId))
}
